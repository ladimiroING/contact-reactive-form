import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'manager-application',
    loadChildren: () => import('../app/ui/contact.components.module').then(components => components.ContactComponentModule)
  },
  {
    path: '**',
    redirectTo: 'manager-application'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
