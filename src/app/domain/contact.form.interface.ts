import { Contact } from "./contact";
import { FormContact } from "./form.contact";

export interface IContactForm {
  contactFormEmpty(): FormContact;
  contactFormCompleted(): Contact;
}
