import { FormControl } from "@angular/forms"

export interface FormContact {
  id?: number,
  name: FormControl<string | null>,
  last_name: FormControl<string | null>,
  phone: FormControl<string | null>,
  email: FormControl<string | null>
}
