import { Observable } from "rxjs";
import { Contact } from "./contact";

export interface IContactService {
  showContacts(): Observable<Contact[]>;
  addContact(contact: Contact): Observable<Contact>;
}
