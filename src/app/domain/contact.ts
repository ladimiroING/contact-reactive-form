export interface Contact {
  id?: number,
  name: string | null,
  last_name: string | null,
  phone: string | null,
  email: string | null
}
