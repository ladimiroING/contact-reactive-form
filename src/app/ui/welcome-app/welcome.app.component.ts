import { Component } from "@angular/core";

@Component({
  selector: 'welcome-app',
  templateUrl: './welcome.app.component.html'
})
export class WelcomeAppComponent {
  title: string = 'Welcome Application'
}
