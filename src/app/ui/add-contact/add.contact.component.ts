import { Component, Inject, OnInit } from "@angular/core";
import { ServicesApplication } from "src/app/common/constants/enum.services.app";
import { IContactForm } from "src/app/domain/contact.form.interface";
import { IContactService } from "src/app/domain/contact.interface.serv";
import { FormContact } from "src/app/domain/form.contact";

@Component({
  selector: 'app-add-contact',
  templateUrl: './add.contact.component.html',
  styleUrls: ['./add.contact.component.css'],
})
export class AddContactComponent implements OnInit {
  form!: FormContact;

  constructor(
    @Inject(ServicesApplication.IContactService)
    private _contactSVC: IContactService,
    @Inject(ServicesApplication.IContactForm)
    private _contactForm: IContactForm
  ) { }

  ngOnInit(): void {
    this.form = this._contactForm.contactFormEmpty();
  }

  sendForm() {
    let contact = this._contactForm.contactFormCompleted();
    console.log(contact.name);
  }
}
