import { NgModule } from '@angular/core';
import { ShowContactsComponent } from './show-contact/show.contacts.component';
import { AddContactComponent } from './add-contact/add.contact.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ProviderContactForm, ProviderContactService } from '../common/providers/providers.app';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ContactComponentRoutingModule } from './contact.component.routing.module';
import { WelcomeAppComponent } from './welcome-app/welcome.app.component';
import { MatInputModule } from '@angular/material/input';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    ShowContactsComponent,
    AddContactComponent,
    WelcomeAppComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatFormFieldModule,
    ContactComponentRoutingModule,
    MatInputModule,
    HttpClientModule
  ],
  providers: [
    ProviderContactService,
    ProviderContactForm
  ]
})
export class ContactComponentModule { }
