import { Injectable } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { FormContact } from "src/app/domain/form.contact";
import { IContactForm } from "src/app/domain/contact.form.interface";
import { Contact } from "src/app/domain/contact";

@Injectable()
export class ContactForm implements IContactForm {
  formEmpty!: FormContact;
  contact!: Contact;

  constructor() {
    this.formEmpty = {
      name: new FormControl<string | null>('', Validators.required),
      last_name: new FormControl<string | null>('', Validators.required),
      phone: new FormControl<string | null>('', [Validators.required, Validators.maxLength(10)]),
      email: new FormControl<string | null>('', [Validators.email, Validators.required])
    }
  }

  contactFormCompleted(): Contact {
    this.contact = {
      name: this.formEmpty.name.value,
      last_name: this.formEmpty.last_name.value,
      phone: this.formEmpty.phone.value,
      email: this.formEmpty.email.value
    }
    return this.contact;
  }

  contactFormEmpty(): FormContact {
    return this.formEmpty;
  }
}
