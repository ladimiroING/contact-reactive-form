import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddContactComponent } from "./add-contact/add.contact.component";
import { ShowContactsComponent } from "./show-contact/show.contacts.component";
import { WelcomeAppComponent } from "./welcome-app/welcome.app.component";

const routes: Routes = [
  {
    path: '',
    children: [
      {path: 'welcome-app', component: WelcomeAppComponent},
      { path: 'add-contact', component: AddContactComponent },
      { path: 'show-contacts', component: ShowContactsComponent },
      { path: '**', redirectTo: 'welcome-app' }
    ]
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class ContactComponentRoutingModule {

}
