import { ContactService } from "src/app/services/contact.service";
import { ServicesApplication } from "../constants/enum.services.app";
import { Provider } from "@angular/core";
import { ContactForm } from "src/app/ui/forms/contact.form";

export const ProviderContactService: Provider = {
  provide: ServicesApplication.IContactService,
  useClass: ContactService
}

export const ProviderContactForm: Provider = {
  provide: ServicesApplication.IContactForm,
  useClass: ContactForm
}
