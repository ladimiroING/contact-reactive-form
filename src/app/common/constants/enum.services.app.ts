import { IContactForm } from 'src/app/domain/contact.form.interface';
import { IContactService } from './../../domain/contact.interface.serv';

export enum ServicesApplication {
  IContactService,
  IContactForm
}
