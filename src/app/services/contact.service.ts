import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { Contact } from "../domain/contact";
import { IContactService } from "../domain/contact.interface.serv";
import { HttpClient } from '@angular/common/http'

@Injectable()
export class ContactService implements IContactService {
  private urlContacts: string = 'http://localhost:3000/contacts';

  constructor(private http: HttpClient) { }

  showContacts(): Observable<Contact[]> {
    throw new Error("Method not implemented.");
  }

  addContact(contact: Contact): Observable<Contact> {
    return this.http.post<Contact>(this.urlContacts, contact);
  }
}
